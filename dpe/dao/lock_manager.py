class LockManager(object):

    def __init__(self):
        self._locks = {}
        self._lock_assignments = {}

    def add_lock(self, name):
        lock = Lock(name)
        self._locks[lock.name] = lock

    def remove_lock(self, name):
        self.release_lock(name)
        del self._locks[name]

    def get_lock(self, name):
        if name not in self._locks:
            self.add_lock(name)
            return self._locks[name]
        return None

    def release_lock(self, name):
        if self._lock_assignments[name]:
            self._lock_assignments[name] = None


class Lock(object):

    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        return self._name
