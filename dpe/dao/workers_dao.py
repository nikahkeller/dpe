import time


class WorkersDAO(object):

    WORKERS = {1: (1, time.time()), 2: (2, time.time()), 3: (3, time.time()), 4: (4, time.time())}

    def __init__(self):
        print("test")

    def add(self, worker_id):
        worker = (worker_id, time.time())
        self.WORKERS[worker_id] = worker

    def remove(self, worker_id):
        del self.WORKERS[worker_id]

    def update_timestamp(self, worker_id):
        worker = list(self.WORKERS[worker_id])
        worker[1] = time.time()
        self.WORKERS[worker_id] = tuple(worker)

    def get_workers(self):
        return self.WORKERS
