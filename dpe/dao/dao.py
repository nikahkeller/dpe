import mysql.connector
from psycopg2.pool import ThreadedConnectionPool


class DPEDAO(object):
    connection_pool = None

    def __init__(self, connection_pool):
        self._connection_pool = connection_pool

    @classmethod
    def create_pool(cls, db_user, db_password, db_host, db_port, db_name):
        try:
            return ThreadedConnectionPool(
                1,
                128,
                user=db_user,
                password=db_password,
                host=db_host,
                port=db_port,
                database=db_name)
        except psycopg2.Error as e:
            raise DatabaseError("Connecting to DB failed.", e)

    @classmethod
    def create_from_env(cls):
        if not cls.connection_pool:
            db_user = "root"
            db_password = "root"
            db_host = "34.71.97.147"
            db_port = "3306"
            db_name = "dpe"
            cls.connection_pool = cls.create_pool(db_user, db_password, db_host, db_port, db_name)
        return cls(cls.connection_pool)

    def get_work_assignment(self, worker_id):
        connection = self._connection_pool.getconn()
        cursor = connection.cursor()
        try:
            cursor.execute("""SELECT * FROM work_assignments WHERE worker_id == %s""", (worker_id, ))
        except Exception as e:
            raise DatabaseError("Failed to get work_assignment.", e)
        db_result = cursor.fetchall()
        work_assignment = [e[1] for e in db_result]
        self._connection_pool.putconn(connection)
        return work_assignment

    def add_worker(self):
        connection = self._connection_pool.getconn()
        cursor = connection.cursor()
        try:
            cursor.execute("""SELECT * FROM work_assignments WHERE worker_id == %s""", (worker_id, ))
        except Exception as e:
            raise DatabaseError("Failed to get work_assignment.", e)


class DatabaseError(Exception):
    """Error from a specific database and host caused by connecting or executing SQL."""
