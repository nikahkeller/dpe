class WorkAssignmentDAO(object):

    ASSIGNMENTS = {1: [1], 2: [2], 3: [3]}

    def __init__(self):
        pass

    def write_assignments(self, assignments):
        pass

    @staticmethod
    def read_assignments(worker_id):
        return WorkAssignmentDAO.ASSIGNMENTS[worker_id]
