from dpe.scheduler import Scheduler
from dpe.worker import Worker
from dpe.dao.work_items_dao import WorkItemsDAO
from dpe.dao.lock_manager import LockManager
from dpe.dao.workers_dao import WorkersDAO
from dpe.dao.work_assignment_dao import WorkAssignmentDAO
import time


def main():
    work_items_dao = WorkItemsDAO()
    lock_manager = LockManager()
    work_assignment_dao = WorkAssignmentDAO()
    workers_dao = WorkersDAO()

    scheduler1 = Scheduler(workers_dao, work_items_dao, lock_manager, work_assignment_dao)
    scheduler1.start()

    scheduler2 = Scheduler(workers_dao, work_items_dao, lock_manager, work_assignment_dao)
    scheduler2.start()

    worker1 = Worker(1, work_assignment_dao, workers_dao)
    worker1.start()

    worker2 = Worker(2, work_assignment_dao, workers_dao)
    worker2.start()

    time.sleep(10)
    scheduler1.stop()
    scheduler2.stop()
    worker1.stop()
    worker2.stop()


if __name__ == '__main__':
    main()
