from datetime import datetime, timedelta
import threading
import time


class Scheduler(object):

    def __init__(self, workers_dao, work_items_dao, lock_manager, work_assignment_dao):
        self._workers_dao = workers_dao
        self._work_items_dao = work_items_dao
        self._lock_manager = lock_manager
        self._work_assignment_dao = work_assignment_dao
        self._should_run = False
        self._thread = threading.Thread(target=self.run, name="scheduler")

    def create_assignments(self):
        workers = self._workers_dao.get_workers()
        work_items = self._work_items_dao.get_items()
        assignments = {workers[1]: work_items[0], workers[2]: work_items[1]}
        self._work_assignment_dao.write_assignments(assignments)

    def run(self):
        while self._should_run:
            lock = self._lock_manager.get_lock("advisory")
            if not lock:
                next_lock_attempt = datetime.now() + timedelta(seconds=60)
                while datetime.now() < next_lock_attempt and self._should_run:
                    time.sleep(1)
                continue
            self.create_assignments()

    def start(self):
        self._should_run = True
        self._thread.start()

    def stop(self):
        self._should_run = False
        self._thread.join()
