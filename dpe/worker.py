from datetime import datetime, timedelta
import threading
import concurrent.futures
import time


class Worker(object):

    def __init__(self, id, work_assignment_dao, workers_dao):
        self._id = id
        self._work_assignment_dao = work_assignment_dao
        self._workers_dao = workers_dao
        self._should_run = False
        self._thread = threading.Thread(target=self.run, name="worker_{}".format(self._id))

    def report_health(self):
        self._workers_dao.update_timestamp(self._id)

    def run(self):
        while self._should_run:
            self.report_health()
            assignments = self._work_assignment_dao.read_assignments(self._id)
            with concurrent.futures.ThreadPoolExecutor(max_workers=len(assignments)) as executor:
                futures = []
                next_work_assignment_read = datetime.now() + timedelta(seconds=60)
                for assignment in assignments:
                    future = executor.submit(self.do_work, assignment, next_work_assignment_read)
                    futures.append(future)

                while datetime.now() < next_work_assignment_read and self._should_run:
                    time.sleep(1)

                if not self._should_run:
                    self.cancel_work(futures)
                    return
                for future in futures:
                    try:
                        future_exception = future.exception(timeout=5)
                    except TimeoutError:
                        print("Timeout")
                        future.cancel()
                    if future_exception:
                        print(future_exception)

    @staticmethod
    def cancel_work(futures):
        for future in futures:
            future.cancel()

    def do_work(self, assignment, work_until):
        while datetime.now() < work_until:
            print("worker {} has assignment {}\n".format(self._id, assignment))
            time.sleep(5)

    def start(self):
        self._should_run = True
        self._thread.start()

    def stop(self):
        self._should_run = False
        self._thread.join()
